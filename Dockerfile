FROM node:10-alpine
RUN apk --no-cache add wget
WORKDIR /app
COPY package*.json /app/
RUN npm install --production
COPY ./src /app/src
COPY ./public /app/public
#VOLUME /build

EXPOSE 5000
HEALTHCHECK --interval=30s --timeout=1s CMD wget localhost:5000/ping -q -O/dev/null || exit 1
ARG hidden
RUN npm run build
USER node
CMD ["npm","run","serve"]
