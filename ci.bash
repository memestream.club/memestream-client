#!/bin/bash
set -e
git pull origin master
NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" 

nvm install
nvm use
npm install
#todo: need someway of not serving content during build time. maybe syslink ? 
npm run build
rm -rf prod-build
mkdir prod-build
cp -r build/** prod-build
