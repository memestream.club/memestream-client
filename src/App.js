import React, { Component } from "react";
import "./App.css";
import "bulma/css/bulma.css";
import openSocket from "socket.io-client";
import { CSSTransitionGroup } from "react-transition-group"; // ES6
import Post from "./post";
const hiddenAddress = "http://pxvwo6gvkk3su5l6.onion";

class App extends Component {
  constructor(props) {
    super(props);
    const consented = !!localStorage.getItem("consented");
    this.state = {
      posts: [],
      endpoint:
        window.location.hostname.indexOf(".onion") > 0
          ? hiddenAddress
          : "https://memestream.club",
      paused: !consented,
      consented
    };
  }
  componentDidMount() {
    const { endpoint } = this.state;
    const socket = openSocket(endpoint, { path: "/ws" }); // this should be closed on umount + listeners removed
    let postQueue = [];

    const doPosts = posts => {
      // keep post queue to 50 latest posts
      // if the page keeps open for a while,
      // posts that since been archived are show in the stream
      // this sucks b/c the image wont load and the link wont work
      let tmp = postQueue.concat(posts);
      postQueue = tmp.slice(Math.max(0, tmp.length - 51), tmp.length - 1);
    };

    socket.on("8chan", doPosts);
    socket.on("4chan", doPosts);

    socket.on("immediate", posts => {
      console.log("immediate", posts);
      if (this.state.paused || this.state.posts.length) {
        return;
      }

      this.setState(
        Object.assign(this.state, {
          posts: posts.map(p =>
            Object.assign({}, p, { __html: p.com, key: Math.random() })
          )
        })
      );
    });

    setInterval(() => {
      if (this.state.paused) {
        return; //nop
      }
      const post = postQueue.shift();

      if (!post) {
        return;
      }
      this.setState(
        Object.assign(this.state, {
          posts: [
            Object.assign({}, post, { __html: post.com, key: Math.random() })
          ]
            .concat(this.state.posts)
            .slice(0, 25)
        })
      );
    }, 1500); // interval should be closed on unmount

    document.addEventListener("keypress", this.handleKeyPress, false);
  }

  componentWillUnmount() {
    document.removeEventListener("keypress", this.handleKeyPress, false);
  }

  handleKeyPress = e => {
    if (e.code === "Space") {
      this.setState(Object.assign(this.state, { paused: !this.state.paused }));
      e.preventDefault();
    }
  };

  consent = () => {
    this.setState(
      Object.assign(this.state, { consented: true, paused: false })
    );
    localStorage.setItem("consented", true);
  };

  pause = () => {
    this.setState(Object.assign(this.state, { paused: !this.state.paused }));
  };

  render() {
    const { posts } = this.state;
    const consent = this.consent;
    const shit = posts.map(post => <Post post={post} key={post.key} />);

    let main;
    if (this.state.consented) {
      main = <div className="App-intro">{shit}</div>;
    } else {
      main = (
        <div class="modal is-active">
          <div class="modal-background" />
          <div class="modal-card">
            <header class="modal-card-head">
              <p class="modal-card-title">
                MEMESTREAM requires explicit consent
              </p>
            </header>
            <section class="modal-card-body">
              this is a live stream of 4chan and 8chan posts. nothing is
              filtered or moderated. there will be NSFW content.
              <quote>
                To access this section of memestream (the "website"), you
                understand and agree to the following: The content of this
                website is for mature audiences only and may not be suitable for
                minors. If you are a minor or it is illegal for you to access
                mature images and language, do not proceed. This website is
                presented to you AS IS, with no warranty, express or implied. By
                clicking "BRING ON THE FILTH" you agree not to hold memestream
                (or 4chan) responsible for any damages from your use of the
                website, and you understand that the content posted is not owned
                or generated by memestream (or 4chan), but rather by 4chan's
                users. As a condition of using this website, you agree to comply
                with the "Rules" of 4chan, which are also linked on 4chan.org .
                Please read the Rules carefully, because they are important.
              </quote>
            </section>
            <footer class="modal-card-foot">
              <button onClick={consent} className="button is-success">
                BRING ON THE FILTH
              </button>
              <button className="button">No thanks</button>
            </footer>
          </div>
        </div>
      );
    }
    return (
      <div className="App">
        <section class="hero">
          <div class="hero-body">
            <div class="container">
              <div class="columns">
                <div class="column">
                  <img src="/favicon-32x32.png" alt="MEMESTREAM.CLUB" />
                  MEMESTEAM.CLUB
                </div>
                <div class="column is-hidden-mobile">
                  <div class="right">
                    <button onClick={this.pause} className="button">
                      press SPACE to
                      {this.state.paused ? " resume" : " pause"}
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <div class="container">{main}</div>
        <button
          className="mdc-fab app-fab--absolute mdc-fab"
          aria-label="Favorite"
          onClick={this.pause}
        >
          <span class="mdc-fab__icon material-icons strong cap">
            {this.state.paused ? "resume" : "pause"}
          </span>
        </button>
        <footer class="footer">
          <div class="content has-text-centered">
            <p>
              MemeStream is <a href="https://unlicense.org/">unlicensed</a>
              (since 2018) and the source can be reviewed at{" "}
              <a href="https://gitlab.com/groups/memestream.club">
                gitlab.com/groups/memestream.club
              </a>{" "}
            </p>
            <p>
              BUT 4Chan.org and 8ch.net are property of their respective owners.
              Additionally, MemeStream does not host any of the content in the
              posts, all content is proxied from their public APIs.
            </p>
            <p>
              send your love and hate to{" "}
              <a href="mailto:memestream@gmx.com">memestream@gmx.com</a>
            </p>
            <p>
              also available as a onion hidden service at{" "}
              <a href="http://pxvwo6gvkk3su5l6.onion">pxvwo6gvkk3su5l6.onion</a>
            </p>
          </div>
        </footer>
      </div>
    );
  }
}

export default App;
