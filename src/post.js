import React, { Component } from "react";
import "./App.css";
import "bulma/css/bulma.css";

class Post extends Component {
  constructor(props) {
    super(props);
    this.state = {
      post: props.post,
      showModal: false
    };
  }
  showImgModal = show => {
    this.setState(Object.assign(this.state, { showModal: show }));
  };
  componentDidMount() {
    document.addEventListener("keypress", this.handleKeyPress, false);
  }
  componentWillUnmount() {
    document.removeEventListener("keypress", this.handleKeyPress, false);
  }

  handleKeyPress = e => {
    console.log(e.code);
    if (e.code === "Escape") {
      this.showImgModal(false);
      e.preventDefault();
    }
  };
  render() {
    const post = this.state.post;
    return (
      <div className="post" key={post.key}>
        <div>
          {post.source} board: /{post.board}/{" "}
        </div>
        <div>
          {this.state.showModal && (
            <div
              onClick={() => this.showImgModal(false)}
              className={"modal " + (!!this.state.showModal ? "is-active" : "")}
            >
              <div className="modal-background" />
              <div className="modal-content">
                <p className="image ">
                  <img src={post.image} alt={post.filename} />
                </p>
              </div>
              <button
                className="modal-close is-large"
                aria-label="close"
                onClick={() => this.showImgModal(false)}
              />
            </div>
          )}
          <a
            className="image"
            onClick={() => this.showImgModal(true)}
            style={{ width: post.tn_w + "px", height: post.tn_h + "px" }}
          >
            <img
              height={post.tn_h}
              width={post.tn_w}
              src={post.thumbImage}
              alt={post.filename}
            />
          </a>
        </div>

        <div dangerouslySetInnerHTML={post} />

        <div>
          <a href={post.post}>post</a>
        </div>
      </div>
    );
  }
}

export default Post;
