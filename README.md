## #memestream client

react js frontend for https://memestreamclub.herokuapp.com/index.html

## requirements

- nvm

## getting started

1. `nvm install`
2. `nvm use`
3. `npm run build`
4. open [localhost:3000](http://localhost:3000)
